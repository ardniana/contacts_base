/**
 * Created by sasha on 01.06.17.
 */
$(document).ready(function() {
    var allUsersTable = $('#allUsersTable').dataTable({
        "bServerSide": false,
        "sAjaxSource": links.ajax_clients_datatable,
        "bProcessing": true,
        "bLengthChange": true,
        "bFilter": false,
        'sDom': 'rtpli',
        "bSortable": false,
        "autoWidth": true,
        "bInfo": true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        "iDisplayLength": 10,
        "aoColumnDefs": [
        ]
    });
});