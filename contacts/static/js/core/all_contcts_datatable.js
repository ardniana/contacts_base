/**
 * Created by sasha on 22.05.17.
 */
$(document).ready(function() {
    var key_modal;
    var allContactsTable = $('#allContactsTable').dataTable({
        "bServerSide": true,
        "sAjaxSource": links.get_all_contacts,
        "bProcessing": true,
        "bLengthChange": true,
        "bFilter": true,
        'sDom': 'rtpli',
        "bSortable": true,
        "autoWidth": true,
        "bInfo": true,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        "iDisplayLength": 10,
        "aoColumnDefs": [
            {
                "mRender": function (data, type, row) {
                    if (data != '') {
                        $('#user_row').show();
                        return data
                    }
                    else {
                        return data
                    }
                },
                "aTargets": [9]
            },
            {
                "mRender": function (data, type, row) {
                    row.status_filter = '<button type="button" style="border-radius: 0" class="btn btn-primary btn-sm edit_modal" data-cid="' + row[0] + '" data-toggle="modal" data-target="#contacts_modal">' +
                            'EDIT</button></div>';

                    return row.status_filter;
                },
                "aTargets": [10],
                "orderable": false
            },
            {
                "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 9],
                "bSortable": false
            },
        ]
    });


    $('#country-filter', this).change( function () {
         allContactsTable.fnFilter($(this).val(), 1);
    });

    $('#city-filter', this).change( function () {
         allContactsTable.fnFilter($(this).val(), 0);
    });

    $('#contacts_modal').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var cid = button.data('cid');
        $("#contact_b_date").datepicker({
                        startDate: '-100y',
                        endDate: '1'
                    });
        $("#contact_b_date").datepicker('formatDate', "yy-mm-dd").datepicker('setDate', "-50y");
        $('#contact_add_date').datepicker('setDate', "{0}d");
        $('#contact_name, #contact_surname, #contact_city, #contact_country, #contact_phone, #contact_email').val(null);
        $('#contact_name_div, #contact_surname_div, #contact_city_div, #country_name_div, #phone_number_div, ' +
            '#email_contact_div, #date_birthday_div, #date_add_div').removeClass('has-error');
        $('#contact_name_help, #contact_surname_help, #contact_city_help, #country_name_help, ' +
            '#phone_number_help, #email_contact_help, #date_birthday_help, #date_add_help').text('');

        if (cid != null) {
            key_modal = true;
            $('#delete_contact').show();
            $('#export_contact').show();
            $('#contact_title_modal').text('Manage Contacts Form')
            $.ajax({
                type: "GET",
                url: links.ajax_contact_change_modal,
                data: {
                    'contact_id': cid
                },
                success: function (data) {
                    $('#contact_id').val(data['id']);
                    $('#contact_name').val(data['contact_name']);
                    $('#contact_surname').val(data['contact_surname']);
                    $('#contact_city').val(data['city_name']);
                    $('#contact_country').val(data['country_name']);
                    $('#contact_phone').val(data['phone_number']);
                    $('#contact_email').val(data['email_contact']);
                    $('#contact_b_date').val(data['date_birthday']);
                    $('#contact_add_date').val(data['date_add']);
                },
            });
        }
        if (cid==null){
            key_modal = false;
            $('#delete_contact').hide();
            $('#export_contact').hide();
            $('#contact_id').val(0)
            $('#contact_title_modal').text('Add Contacts Form');
        }
    });

    $("#delete_contact").click(function () {
        if (confirm("Are you sure, that you want delete this version report?")) {
            var data = {'contact_id': $('#contact_id').val()};
            var csrftoken = getCookie('csrftoken');
            $.ajax({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                type: "POST",
                url: links.ajax_delete_contact_modal,
                data: data,
                success: function (data) {
                    if (data['success']) {
                        alert('Deleted!');
                        $('#contacts_modal').modal('hide');
                         allContactsTable.api().ajax.reload(null, false);
                    } else {
                        alert('Not deleted!');
                    }
                },
            });
        }
    });

    $("#new_contact").click(function () {
        var data = {
            'id': $('#contact_id').val(),
            'contact_name': $('#contact_name').val(),
            'contact_surname': $('#contact_surname').val(),
            'city_name': $('#contact_city').val(),
            'country_name': $('#contact_country').val(),
            'phone_number': $('#contact_phone').val(),
            'email_contact': $('#contact_email').val(),
            'date_birthday': $('#contact_b_date').val(),
            'date_add': $('#contact_add_date').val()
        };
        $('#contact_name_div, #contact_surname_div, #contact_city_div, #country_name_div, #phone_number_div, ' +
            '#email_contact_div, #date_birthday_div, #date_add_div').removeClass('has-error');
        $('#contact_name_help, #contact_surname_help, #contact_city_help, #country_name_help, ' +
            '#phone_number_help, #email_contact_help, #date_birthday_help, #date_add_help').text('');

        var csrftoken = getCookie('csrftoken');
        if (key_modal==false) {
            $.ajax({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                type: "POST",
                url: links.ajax_add_contact_modal,
                data: data,
                success: function (data) {
                    if (!(data['success'])) {
                        data['errors'].forEach(function (item, i, arr) {
                            $('#' + item[0] + '_div').addClass('has-error');
                            $('#' + item[0] + '_help').text(item[1]);
                        });
                        alert('Not add new contact.')
                    } else {
                        alert('Added new contact.');
                        $('#contacts_modal').modal('hide');
                        allContactsTable.api().ajax.reload(null, false);
                    }
                },
            });
        }
        else {
            $.ajax({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                type: "POST",
                url: links.ajax_contact_change_modal,
                data: data,
                success: function (data) {
                    if (!(data['success'])) {
                        data['errors'].forEach(function (item, i, arr) {
                            $('#' + item[0] + '_div').addClass('has-error');
                            $('#' + item[0] + '_help').text(item[1]);
                        });
                        alert('Not changed contact.')
                    } else {
                        alert('Contact changed.');
                        $('#contacts_modal').modal('hide');
                        allContactsTable.api().ajax.reload(null, false);
                    }
                },
            });
        }

    });

    $('#export_contact').click(function() {
        var data = {'contact_id': $('#contact_id').val()};
        $.ajax({
            type: "GET",
            url: links.ajax_add_contact_vero,
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data['success']) {
                    alert('Contact exported in GetVero');
                    $('#contacts_modal').modal('hide');
                    allContactsTable.api().ajax.reload(null, false);
                }
            }
        });
    });

    $('#export_vero').click(function() {
        var data = {'contact_id': null};
        $('#question').hide();
        $('#export_vero').hide();
        $('#overlay').modal('show');
        $.ajax({
            type: "GET",
            url: links.ajax_add_contact_vero,
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data['success']) {
                    $('#overlay').modal('hide');
                    $('#question').show();
                    $('#question').text('Contacts exported in GetVEro');
                    // alert('Contacts exported in GetVero');
                    allContactsTable.api().ajax.reload(null, false);
                }
            }
        });
    });

    $('#vero_contact_add').click(function() {
        $('#question').show();
        $('#export_vero').show();
        $('#question').text('Are you sure that you want to export contacts to GetVero?');

    });

    $('#csv_contact_add').click(function() {
        $('#upload_contacts').show();
        $('#csv_file_name').hide();
        $('#import_csv').hide();
    });

    function upload_csv_file(event) {
        event.preventDefault();
        var data = new FormData($('form').get(0));
        var file = $('#csvFile').get()[0].files[0];
        data.append("csvFile", file);
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function () {
                alert('Upload')
                    $('#csv_file_name').show();
                    $('#upload_contacts').hide();
                    $('#import_csv').show();
            }
        });
    }

    $('#upload_form_csv_file').submit(upload_csv_file);

    $('#import_csv').click(function() {
        $.ajax({
            type: "GET",
            url: links.upload_csv_file,
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#import_csv').hide();
                $('#uploadCsvForm').show();
                if (data['success'].length==0) {
                    alert('Not add contacts.');
                }
                else {
                    if (data['errors']==0) {
                        alert('All contacts add.');
                    }
                    else {
                        alert('Contacts added count - '+ data['success'].length+ '\nContact error - '+ data['errors']);
                    }
                    allContactsTable.api().ajax.reload(null, false);
                }
            },
        });
    });

});

