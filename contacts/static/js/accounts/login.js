/**
 * Created by sasha on 21.05.17.
 */
$(function () {
    $('input:checkbox').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});