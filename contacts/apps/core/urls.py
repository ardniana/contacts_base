from django.conf.urls import url
from django.contrib.auth.decorators import login_required, permission_required
from contacts.apps.core.views import AllContacts,  GetAllContacts, DeleteContact, AddContact, AjaxContactChangeModal,\
    GetAllUser, AllUsers, AddContactCsv, Vero

urlpatterns = [
    url(r'^$', AllContacts.as_view(), name='all_contacts'),
    url(r'^ajax_get_all_contacts/$', GetAllContacts.as_view(), name='ajax_get_all_contacts'),
    url(r'^all_users/$', AllUsers.as_view(), name='all_users'),
    url(r'^ajax_clients_datatable/$', GetAllUser.as_view(), name='ajax_clients_datatable'),
    url(r'^ajax_contact_change_modal/$', AjaxContactChangeModal.as_view(), name='ajax_contact_change_modal'),
    url(r'^ajax_delete_contact_modal/$', DeleteContact.as_view(), name='ajax_delete_contact_modal'),
    url(r'^ajax_add_contact_modal/$', AddContact.as_view(), name='ajax_add_contact_modal'),
    url(r'^upload_csv_file/$', AddContactCsv.as_view(), name='upload_csv_file'),
    url(r'^ajax_add_contact_vero/$', Vero.as_view(), name='ajax_add_contact_vero'),

]