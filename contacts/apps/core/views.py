# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import json
import csv
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.contrib.auth.models import User
from contacts.apps.core.models import Contacts
from contacts.apps.accounts.models import UserProfile
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from datetime import datetime
from contacts.apps.core.forms import ContactAddForm
from contacts.settings import BASE_DIR
from contacts.apps.core.tools import email, date_validate, export_contact



@method_decorator(login_required, name='dispatch')
class AllContacts(View):
    template_name = 'all_contacts.html'

    def get(self, request, *args, **kwargs):
        active_city = []
        active_country = []
        if request.user.is_superuser:
            contacts_filter = Contacts.objects.all().order_by('-id')
        else:
            user_profile = UserProfile.objects.filter(user=request.user)
            contacts_filter = Contacts.objects.filter(user_profile=user_profile)
        for row in contacts_filter:
            if not row.city_name in active_city:
                active_city.append(row.city_name)
            if not row.country_name in active_country:
                active_country.append(row.country_name)
        data = {
            'city_name': active_city,
            'country_name': active_country,
            'today': datetime.today().strftime('%Y-%m-%d')
        }
        return render(request, self.template_name, data)


@method_decorator(login_required, name='dispatch')
class GetAllContacts(View):

    def get(self, request, *args, **kwargs):
        user_profile = UserProfile.objects.get(user=request.user)
        ajax_response = {'sEcho': '', 'aaData': [], 'iTotalRecords': 0, 'iTotalDisplayRecords': 0}
        if request.user.is_superuser:
            contacts = Contacts.objects.all().order_by('-id')
        else:
            contacts = Contacts.objects.filter(user_profile=user_profile).order_by('-contact_name')
        # filters
        city_filter = request.GET.get('sSearch_0')
        country_filter = request.GET.get('sSearch_1')
        if city_filter and city_filter != '0':
            contacts = contacts.filter(city_name=city_filter)
        if country_filter and country_filter != '0':
            contacts = contacts.filter(country_name=country_filter)
        sort_data = request.GET.get('iSortCol_0')
        metod_sort = request.GET.get('sSortDir_0')
        char_sort = ''
        if metod_sort == 'desc': char_sort = '-'
        if sort_data == '8':
            contacts = contacts.order_by(char_sort + 'date_add')
        # getting lens of response
        ajax_response['iTotalRecords'] = len(contacts)
        ajax_response['iTotalDisplayRecords'] = len(contacts)
        start = int(request.GET['iDisplayStart'])
        length = int(request.GET['iDisplayLength'])
        for contact in contacts[start:start + length]:
            if request.user.is_superuser:
                user = str(contact.user_profile)
            else:
                user = ''
            ajax_response['aaData'].append([
                contact.id,
                contact.contact_name,
                contact.contact_surname,
                contact.city_name,
                contact.country_name,
                contact.phone_number,
                contact.email_contact,
                contact.date_birthday.strftime('%Y-%m-%d'),
                contact.date_add.strftime('%Y-%m-%d'),
                user
            ])
        return HttpResponse(json.dumps(ajax_response), content_type='application/json')


@method_decorator(login_required, name='dispatch')
class AjaxContactChangeModal(View):
    template_name ='all_contacts.html'

    def get(self, request, *args, **kwargs):
        contact = Contacts.objects.get(pk=request.GET['contact_id'])
        form = ContactAddForm(instance=contact)
        return HttpResponse(json.dumps(dict([(str(k), str(v)) for k, v in form.initial.items()])),
                            content_type='application/json')

    def post(self, request, *args, **kwargs):
        contact = Contacts.objects.get(pk=request.POST['id'])
        form = ContactAddForm(request.POST, instance=contact)
        if form.is_valid():
            form.save()
            return HttpResponse(json.dumps({'success': True}),
                                content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False,
                                            'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                content_type='application/json')


@method_decorator(login_required, name='dispatch')
class DeleteContact(View):

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            contact = Contacts.objects.get(pk=request.POST['contact_id'])
            contact.delete()
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')


@method_decorator(login_required, name='dispatch')
class AddContact(View):

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            form = ContactAddForm(request.POST)
            user_profile = UserProfile.objects.get(user=request.user)
            if form.is_valid():
                contact = form.save(commit=False)
                contact.user_profile = user_profile
                contact.save()
                return HttpResponse(json.dumps({'success': True}),
                                    content_type='application/json')
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type='application/json')
        else:
            return HttpResponse(json.dumps({'success': False}), content_type='application/json')


@method_decorator(login_required, name='dispatch')
class AddContactCsv(View):

    def post(self, request, *args, **kwargs):
        csvfile = request.FILES['csvFile']
        fs = FileSystemStorage()
        filename = fs.save('contacts_%s.csv' % str(Contacts.objects.all().last().id + 1), csvfile)
        uploaded_file_url = fs.url(filename)
        return HttpResponse(json.dumps({'file': uploaded_file_url}))

    def get(self, request, *args, **kwargs):
        user_profile = UserProfile.objects.get(user=request.user)
        input_file = open(BASE_DIR + '/media/contacts_%s.csv' % str(Contacts.objects.all().last().id + 1),
                          'rb')
        csv_contacts = csv.DictReader(input_file, fieldnames=['contact_name', 'contact_surname', 'city_name',
                                                              'country_name', 'phone_number', 'email_contact',
                                                              'date_birthday'])
        i = 0
        errors = {}
        success = []
        for row in csv_contacts:
            i += 1
            error = []
            if row['contact_name'] and row['contact_surname'] and row['email_contact']:
                if not email(row['email_contact']):
                    error.append(['Email error'])
                if len(row['phone_number']) > 15:
                    error.append(['Phone symbol error'])
                if not date_validate(row['date_birthday']):
                    error.append(['Incorrect data format, should be YYYY-MM-DD'])
            else:
                error.append(['Input error'])
            if len(error) > 0:
                errors[i] = error
            else:
                contact_data = Contacts(contact_name=row['contact_surname'],
                                        contact_surname=row['contact_surname'],
                                        city_name=row['city_name'],
                                        country_name=row['country_name'],
                                        phone_number=row['phone_number'],
                                        email_contact=row['email_contact'],
                                        date_birthday=row['date_birthday'],
                                        date_add=datetime.today().strftime('%Y-%m-%d'),
                                        user_profile=user_profile)
                contact_data.save()
                success.append(i)
                # Contacts.objects.bulk_create(contact_data)
        return HttpResponse(json.dumps({'success': success,'errors': len(errors)}),
                            content_type='application/json')


@method_decorator(login_required, name='dispatch')
class Vero(View):

    def get(self, request, *args, **kwargs):
        user_profile = UserProfile.objects.get(user=request.user)
        user_api = user_profile.vero_api_key
        if request.GET['contact_id']:
            contact = Contacts.objects.get(pk=request.GET['contact_id'])
            export_contact(user_api, contact)
        else:
            contacts = Contacts.objects.filter(user_profile=user_profile)
            for contact in contacts:
                export_contact(user_api, contact)
        return HttpResponse(json.dumps({'success': True}),
                            content_type='application/json')


@method_decorator(login_required, name='dispatch')
class AllUsers(View):
    template_name = 'all_users.html'

    def get(self, request, *args, **kwargs):
        data = {}
        return render(request, self.template_name, data)


@method_decorator(login_required, name='dispatch')
class GetAllUser(View):

    def get(self, request, *args, **kwargs):
        ajax_response = {'aaData': []}
        if request.user.is_superuser:
            ajax_response = {'aaData': []}
            for user in User.objects.all():
                ajax_response['aaData'].append([
                    user.pk,
                    user.username,
                    user.first_name,
                    user.last_name,
                    user.email,
                    user.profile.phone_number,
                    '*********%s' % user.profile.vero_api_key[-4:]
                ])
        return HttpResponse(json.dumps(ajax_response), content_type='application/json')

