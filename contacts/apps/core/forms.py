from django import forms
from django.forms import ModelForm
from contacts.apps.core.models import Contacts


class ContactAddForm(ModelForm):
    class Meta:
        model = Contacts
        fields = ['id', 'contact_name', 'contact_surname', 'city_name', 'country_name',
                  'phone_number', 'email_contact', 'date_birthday', 'date_add']

    def clean(self):
        form_data = self.cleaned_data
        return form_data
