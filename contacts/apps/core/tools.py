import requests
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from datetime import datetime


def email(value):
    try:
        validate_email(value)
        return True
    except ValidationError, e:
        return False


def date_validate(date_text):
    try:
        datetime.strptime(date_text, '%Y-%m-%d')
        return True
    except ValueError:
        return False


def export_contact(user_api, contact):
    requests.request('POST', 'https://api.getvero.com/api/v2/users/track',
                     json={'auth_token': user_api, 'id': contact.id, 'email': contact.email_contact,
                           'data': {
                               'first_name': contact.contact_name,
                               'last_name': contact.contact_surname,
                               'country': contact.country_name,
                               'city': contact.city_name,
                               'phone': contact.phone_number,
                               'date birthday': contact.date_birthday.strftime('%Y-%m-%d'),
                           }
                           })