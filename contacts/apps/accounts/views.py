# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
import json
import logging
import traceback
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseServerError, HttpResponseRedirect
from django.views.generic import View
from django.views.generic.edit import FormView
from contacts.apps.accounts.forms import SettingsForm, SettingsProfileForm, ChangePasswordForm, \
    SignupUserForm, SignupUserProfileForm

from contacts.apps.accounts.models import UserProfile

logger = logging.getLogger('contacts')


class LoginPage(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('core:all_contacts'))
        return render(request, self.template_name, {'error': False})

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        logger.debug('Authenticating user %s with password: %s' % (username, password))
        if User.objects.filter(email=username):
            username = User.objects.get(email=username).username
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('core:all_contacts'))
        else:
            logger.debug('User not authenticated. Wrong login(%s)/password(%s).' % (request.POST['username'],
                                                                                    request.POST['password']))
            return render(request, self.template_name, {'error': True,
                                                        'message': 'User not authenticated. Wrong login/password'})


@method_decorator(login_required, name='dispatch')
class SettingsPage(View):
    template_name = 'settings.html'

    def get(self, request, *args, **kwargs):
        try:
            form = SettingsForm(instance=request.user)
            form_profile = SettingsProfileForm(instance=request.user.profile)
        except Exception as e:
            logger.error('Exception: %s. Trace: %s.' % (e, traceback.format_exc(limit=10)))
        else:
            return render(request, self.template_name, {'form': form,
                                                        'form_profile': form_profile,
                                                        'updated': False})

    def post(self, request, *args, **kwargs):
        try:
            updated = False
            form = SettingsForm(request.POST, instance=request.user)
            form_profile = SettingsProfileForm(request.POST, instance=request.user.profile)
            if form.is_valid() and form_profile.is_valid():
                form.save()
                form_profile.save()
                updated = True
        except Exception as e:
            print e
            logger.error('Exception: %s. Trace: %s.' % (e, traceback.format_exc(limit=10)))
            return HttpResponseServerError()
        else:
            return render(request, self.template_name, {'form': form,
                                                        'form_profile': form_profile,
                                                        'updated': updated})


class ChangePasswordView(View):
    template_name = 'change_password_form.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        try:
            data = {}
        except Exception as e:
            logger.error('Exception: %s. Trace: %s.' % (e, traceback.format_exc(limit=10)))
        else:
            return render(request, self.template_name, data)

    def post(self, request, *args, **kwargs):
        try:
            form = ChangePasswordForm(request.POST, instance=request.user)
            if form.is_valid():
                user = request.user
                user.set_password(form.cleaned_data['new_password'])
                user.save()
                return HttpResponse(json.dumps({'success': True}),
                                    content_type='application/json')
            else:
                return HttpResponse(json.dumps({'success': False,
                                                'errors': [(k, v[0]) for k, v in form.errors.items()]}),
                                    content_type='application/json')
        except Exception as e:
            logger.error('Exception: %s. Trace: %s.' % (e, traceback.format_exc(limit=10)))


class RegisterFormView(FormView):
    template_name = 'register.html'

    def get(self, request, *args, **kwargs):
        form = SignupUserForm()
        form_profile = SignupUserProfileForm()
        context = {'form': form,
                   'form_profile': form_profile}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        updated = False
        try:
            form = SignupUserForm(request.POST)
            form_profile = SignupUserProfileForm(request.POST)
            if form.is_valid() and form_profile.is_valid():
                # Create a new user object but avoid saving it yet
                new_user = form.save(commit=False)
                # Set the chosen password
                new_user.set_password(form.cleaned_data['password'])
                # Save the User object
                new_user.save()
                user_profile = form_profile.save(commit=False)
                user_profile.user = new_user
                user_profile.save()
                updated = True
                template_name1 = 'login.html'
                return render(request, template_name1, {'updated': updated,})
                # return HttpResponseRedirect(reverse('accounts:login'))
        except Exception as e:
            print e
            logger.error('Exception: %s. Trace: %s.' % (e, traceback.format_exc(limit=10)))
        else:
            return render(request, self.template_name, {'form': form,
                                                        'form_profile':form_profile})

