from django.conf.urls import url
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.views import logout

from contacts.apps.accounts.views import LoginPage, SettingsPage, ChangePasswordView, RegisterFormView

urlpatterns = [
    url(r'^login/$', LoginPage.as_view(), name='login'),
    url(r'^logout/$', logout, {'next_page': '/login/'}, name='logout'),
    url(r'^register/$', RegisterFormView.as_view(), name='register'),
    url(r'^settings/$', SettingsPage.as_view(), name='settings'),
    url(r'^change_password/$', ChangePasswordView.as_view(),name='change_password'),
]
