import re
from django import forms
import requests
from contacts.apps.accounts.models import UserProfile
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.contrib.auth import authenticate


class SettingsForm(ModelForm):
    class Meta:
        model = User
        fields = ['username',  'first_name', 'last_name', 'email']

    def clean(self):
        form_data = super(SettingsForm, self).clean()
        if not self.is_valid():
            return form_data
        return form_data


class SettingsProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = ['phone_number', 'vero_api_key']

    def __init__(self, *args, **kwargs):
        super(SettingsProfileForm, self).__init__(*args, **kwargs)



class ChangePasswordForm(ModelForm):
    class Meta:
        model = User
        fields = []

    old_password = forms.CharField(required=True)
    new_password = forms.CharField(required=True)
    password_confirmation = forms.CharField(required=True)

    def clean(self):
        form_data = self.cleaned_data
        if not 'old_password' in form_data or not 'new_password' in form_data \
                or not 'password_confirmation' in form_data:
            self._errors['old_password'] = self.error_class(['Field is required'])
            self._errors['new_password'] = self.error_class(['Field is required'])
            self._errors['password_confirmation'] = self.error_class(['Field is required'])
            return form_data
        user = authenticate(username=self.instance.username, password=form_data['old_password'])
        if user is None:
            self._errors['old_password'] = self.error_class(['Wrong password'])
        if form_data['new_password'] != form_data['password_confirmation']:
            self._errors['new_password'] = self.error_class(['Passwords don\'t match'])
            self._errors['password_confirmation'] = self.error_class(['Passwords don\'t match'])
        if not re.match(r'[A-Za-z0-9@#$%^&+=]{8,32}', form_data['new_password']):
            self._errors['new_password'] = self.error_class(['Password must contain uppercase and lowercase '
                                                             'letters and has at least eight (8) characters '
                                                             'but no more than thirty two (32) characters.'])
        return form_data


class SignupUserForm(ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def clean(self):
        form_data = super(SignupUserForm, self).clean()
        no_required_data = False
        if not self.is_valid():
            no_required_data = True
        if User.objects.filter(username=form_data['username']):
            self._errors['username'] = self.error_class(['User with this username already exists.'])
            no_required_data = True
        if User.objects.filter(email=form_data['email']):
            self._errors['email'] = self.error_class(['User with this email already exists.'])
            no_required_data = True
        if self.cleaned_data['password'] != self.cleaned_data['password2']:
            self._errors['password'] = self.error_class(['Passwords don\'t match'])
            self._errors['password2'] = self.error_class(['Passwords don\'t match'])
            no_required_data = True
        if not re.match(r'[A-Za-z0-9@#$%^&+=]{8,32}', self.cleaned_data['password']):
            self._errors['password'] = self.error_class(['Password must contain uppercase and lowercase '
                                                          'letters and has at least eight (8) characters '
                                                          'but no more than thirty two (32) characters.'])
            no_required_data = True
        if no_required_data == True:
            return form_data


class SignupUserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = ['phone_number', 'vero_api_key']

    def __init__(self, *args, **kwargs):

        super(SignupUserProfileForm, self).__init__(*args, **kwargs)
    def __str__(self):

        return 'Profile for user {}'.format(self.user.username)

    def clean(self):
        form_data = super(SignupUserProfileForm, self).clean()
        if self.cleaned_data.get('vero_api_key', None):
            r = requests.request('POST', 'https://api.getvero.com/api/v2/users/track',
                             json={'auth_token': form_data['vero_api_key'], 'id': 'test', 'email': '',
                                   })
            if r.status_code == 401:
                self._errors['vero_api_key'] = self.error_class(['Your credentials are invalid!!!'])
                return form_data
