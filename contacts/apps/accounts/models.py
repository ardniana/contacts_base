# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class UserProfile(models.Model):
    def __init__(self, *args, **kwargs):
        super(UserProfile, self).__init__(*args, **kwargs)

    user = models.OneToOneField(User, related_name='profile')
    phone_number = models.CharField(max_length=15)
    vero_api_key = models.CharField(max_length=109)

    def __unicode__(self):
        return self.user.username
